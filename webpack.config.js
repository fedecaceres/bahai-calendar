var webpack = require('webpack');

const path = require('path');
const WorkboxPlugin = require('workbox-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin')
var CopyWebpackPlugin = require('copy-webpack-plugin');
var WebpackPwaManifest = require('webpack-pwa-manifest')

const DIST_DIR = __dirname + "/dist"

const prodTransform = (manifestEntries) => manifestEntries.map(entry => {
    if (process.env.NODE_ENV == 'production') {
        entry.url = '/bahai-calendar/' + entry.url;
    }
    return entry;
});

module.exports = {
    entry: "./src/app.ts",
    output: {
        // filename: "bundle.min.js",
        filename: "bundle.js",
        path: DIST_DIR
    },

    // Enable sourcemaps for debugging webpack's output.
    devtool: "source-map",

    resolve: {
        // Add '.ts' as resolvable extensions.
        extensions: [".ts", ".js", ".json"]
    },

    module: {
        rules: [
            // All files with a '.ts' extension will be handled by 'awesome-typescript-loader'.
            { test: /\.ts$/, loader: "awesome-typescript-loader" },

            // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
            { enforce: "pre", test: /\.js$/, loader: "source-map-loader" }
        ],
        loaders: [
            {
                test: /\.html$/,
                loader: 'html-loader'
            }],
    },

    plugins: [
        // new webpack.optimize.UglifyJsPlugin({
        //     // minimize: true,
        //     sourceMap: true,
        //     compress: {
        //         warnings: false,
        //         drop_console: false,
        //     }
        // }),
        new CopyWebpackPlugin([{
            from: 'img',
            to: 'img'
        },{
            from: 'node_modules/material-design-lite/dist/material.indigo-blue.min.css',
            to : 'css'
        },{
            from: 'node_modules/material-design-lite/dist/material.min.js',
            to: 'js'
            
        }, {
            from: 'node_modules/material-design-icons/iconfont/*',
            flatten: true,
            ignore: ['.*md'],
            to: 'fonts'
        }]),
        new HtmlWebpackPlugin({
            template: 'src/index.html'
        }),
        new WebpackPwaManifest({
            name: 'Bahá\'i Calendar',
            short_name: 'Bahá\'i Calendar',
            description: 'Bahá\'i Calendar!',
            background_color: '#ffffff',
            icons: [
                {
                    src: path.resolve('img/color-star.png'),
                    sizes: [96, 128, 192, 256, 384, 512], // multiple sizes 
                    destination: 'icons'
                }
            ]
        }),

        new WorkboxPlugin({
            globDirectory: DIST_DIR,
            globPatterns: ['**/*.{html,js,css,eot,ijmap,svg,ttf,woff,woff2}'],
            manifestTransforms: [prodTransform],
            swDest: path.join(DIST_DIR, 'sw.js')
            // instructions on runtimecaching here: https://workboxjs.org/reference-docs/latest/module-workbox-build.html
            // look for runtimeCaching
            // also, interesting article: https://medium.com/musings-in-the-clouds/from-sw-precache-to-workbox-23c9079d14de
            // but... this might not work as expected due to cross origin errors with CDN requests:
            // https://workboxjs.org/how_tos/cdn-caching.html
            // runtimeCaching: [{
            //     urlPattern: /^https:\/\/fonts.googleapis.com\//,
            //     handler: 'cacheFirst'
            // },{
            //     urlPattern: /^https:\/\/fonts.gstatic.com\//,
            //     handler: 'cacheFirst'
            // }]
        }),
    ]

    // When importing a module whose path matches one of the following, just
    // assume a corresponding global variable exists and use that instead.
    // This is important because it allows us to avoid bundling all of our
    // dependencies, which allows browsers to cache those libraries between builds.
    // externals: {
    //     "react": "React",
    //     "react-dom": "ReactDOM"
    // },
};