import { BadiDate } from '../src/calendar';

class GregorianToBadiMatch {
    badiDate: BadiDate;
    gregorianDate: Date;

    constructor(gregorianDate: Date, badiDate: BadiDate) {
        this.badiDate = badiDate;
        this.gregorianDate = gregorianDate;
    }
}

class AyyamIHaMatch {
    badiYear: number;
    firstGregorianDate: Date;
    numberOfIntercalaryDays:number;

    constructor(gregorianDate: Date, badiYear: number, numberOfIntercalaryDays : number) {
        this.badiYear = badiYear;
        this.firstGregorianDate = gregorianDate;
        this.numberOfIntercalaryDays = numberOfIntercalaryDays;
    }
}

describe('Calendar', () => {

    it ('can convert back and forth from Gregorian Date without loosing the source date (from Jan 2005 to Jun 2073)', () => {
        let date = new Date(Date.UTC(2005, 0, 1, 18, 1, 2));
        let originalDate: Date, badiDate: BadiDate, resultDate: Date;
        for (let i = 0; i < 50000; i++) {
            originalDate = new Date(date.getTime() + i * 1000 * 60 * 60 * 12);
            badiDate = BadiDate.fromDateToBadiDate(originalDate);
            resultDate = badiDate.toDate();
            expect(originalDate.getTime()).toBe(resultDate.getTime());
        }
    });

    it ('can conver back and forth from Badi Date without loosing the source date', () => {
        let original = new BadiDate(172, 1, 1);
        let originalGregorians = new Date(2015, 3 - 1, 20, 18, 0, 0);
        let final = BadiDate.fromDateToBadiDate(original.toDate());
        expect(original.toString()).toBe(final.toString());
        
        // console.log(original.toString());
        // expect(original.getTime()).toBe(originalGregorian.getTime());
        // expect(original.toDate().toISOString()).toBe(originalGregorian.toISOString());
    })

    // let myDate = new Date(2018, 3 - 1, 18, 17, 0, 0);
    // for (let hours = 0; hours < 48; hours++) {
    //     let badiDate = BadiDate.fromDateToBadiDate(myDate);
    //     xit('makes secuential sense ' + myDate.toISOString().slice(0, -8) + ' => ' + badiDate.getTextualRepresentation(), () => {
    //         expect(myDate).toBe(new Date(badiDate.getTime()));
    //     })
    //     myDate.addHours(1)
    // }

    let nawRuzYearDays = [
        [2015, 21],
        [2016, 20],
        [2017, 20],
        [2018, 21],
        [2019, 21],
        [2020, 20],
        [2021, 20],
        [2022, 21],
        [2023, 21],
        [2024, 20],
        [2025, 20],
    ];

    for (let pair of nawRuzYearDays) {
        it('calculates nawruz correctly for ' + pair[0], () => {
            // BadiDate returns the day BEFORE (that is, if NawRuz starts on the 21th, it returns 20th at 18:00)
            // that's why we add one to the date
            expect(BadiDate.getNawRuzForGregorianYear(pair[0]).getDate() + 1).toBe(pair[1]);
        })
    }

    let nawRuzDates = [
        // this should not match!!!
        new GregorianToBadiMatch(new Date(2015, 3 - 1, 20, 18, 0, 0), new BadiDate(172, 1, 1)),
        new GregorianToBadiMatch(new Date(2016, 3 - 1, 19, 18, 0, 0), new BadiDate(173, 1, 1)),
        new GregorianToBadiMatch(new Date(2017, 3 - 1, 19, 18, 0, 0), new BadiDate(174, 1, 1)),

        new GregorianToBadiMatch(new Date(2018, 3 - 1, 20, 18, 0, 0), new BadiDate(175, 1, 1)),
        new GregorianToBadiMatch(new Date(2019, 3 - 1, 20, 18, 0, 0), new BadiDate(176, 1, 1)),
        new GregorianToBadiMatch(new Date(2020, 3 - 1, 19, 18, 0, 0), new BadiDate(177, 1, 1)),
        new GregorianToBadiMatch(new Date(2021, 3 - 1, 19, 18, 0, 0), new BadiDate(178, 1, 1)),
        new GregorianToBadiMatch(new Date(2022, 3 - 1, 20, 18, 0, 0), new BadiDate(179, 1, 1)),
        new GregorianToBadiMatch(new Date(2023, 3 - 1, 20, 18, 0, 0), new BadiDate(180, 1, 1)),
        new GregorianToBadiMatch(new Date(2024, 3 - 1, 19, 18, 0, 0), new BadiDate(181, 1, 1)),
        new GregorianToBadiMatch(new Date(2025, 3 - 1, 19, 18, 0, 0), new BadiDate(182, 1, 1)),
        new GregorianToBadiMatch(new Date(2026, 3 - 1, 20, 18, 0, 0), new BadiDate(183, 1, 1)),
        new GregorianToBadiMatch(new Date(2027, 3 - 1, 20, 18, 0, 0), new BadiDate(184, 1, 1)),
        new GregorianToBadiMatch(new Date(2028, 3 - 1, 19, 18, 0, 0), new BadiDate(185, 1, 1)),
        new GregorianToBadiMatch(new Date(2029, 3 - 1, 19, 18, 0, 0), new BadiDate(186, 1, 1)),
        new GregorianToBadiMatch(new Date(2030, 3 - 1, 19, 18, 0, 0), new BadiDate(187, 1, 1)),
        new GregorianToBadiMatch(new Date(2031, 3 - 1, 20, 18, 0, 0), new BadiDate(188, 1, 1)),
        new GregorianToBadiMatch(new Date(2032, 3 - 1, 19, 18, 0, 0), new BadiDate(189, 1, 1)),
        new GregorianToBadiMatch(new Date(2033, 3 - 1, 19, 18, 0, 0), new BadiDate(190, 1, 1)),
        new GregorianToBadiMatch(new Date(2034, 3 - 1, 19, 18, 0, 0), new BadiDate(191, 1, 1)),
        new GregorianToBadiMatch(new Date(2035, 3 - 1, 20, 18, 0, 0), new BadiDate(192, 1, 1)),
    ];

    for (let entry of nawRuzDates) {
        xit('matches gregorian date ' + entry.gregorianDate.toISOString().slice(0, -8) + ' to Naw Ruz for ' + entry.badiDate.getYear() + ' B.E.', () => {
            let badiDateString = entry.badiDate.toString().split(' ')[0];
            // one millisecond before should not match with Naw Ruz
            let calculatedBadiDateNotForNawRuz = BadiDate.fromDateToBadiDate(new Date(entry.gregorianDate.getTime() - 1));
            // console.error('hello');
            // console.log(calculatedBadiDateNotForNawRuz.toString().split(' ')[0], ' != ', badiDateString);
            expect(calculatedBadiDateNotForNawRuz.toString().split(' ')[0]).not.toBe(badiDateString);

            // Naw Ruz should match the exact date given
            let calculatedBadiDateForNawRuz = BadiDate.fromDateToBadiDate(entry.gregorianDate);
            expect(calculatedBadiDateForNawRuz.toString().split(' ')[0]).toBe(badiDateString);
        });
    }

    xit('calculates all 19 days feasts correctly');

    let ayyamIHaDays: AyyamIHaMatch[] = [
        new AyyamIHaMatch(new Date(2016, 2 - 1, 25, 18, 0, 0), 172, 4),
        new AyyamIHaMatch(new Date(2017, 2 - 1, 24, 18, 0, 0), 173, 4),
        new AyyamIHaMatch(new Date(2018, 2 - 1, 24, 18, 0, 0), 174, 5),
        new AyyamIHaMatch(new Date(2019, 2 - 1, 25, 18, 0, 0), 175, 4),
        new AyyamIHaMatch(new Date(2020, 2 - 1, 25, 18, 0, 0), 176, 4),
        new AyyamIHaMatch(new Date(2021, 2 - 1, 24, 18, 0, 0), 177, 4),
        new AyyamIHaMatch(new Date(2022, 2 - 1, 24, 18, 0, 0), 178, 5),
        new AyyamIHaMatch(new Date(2023, 2 - 1, 25, 18, 0, 0), 179, 4),
        new AyyamIHaMatch(new Date(2024, 2 - 1, 25, 18, 0, 0), 180, 4),
        new AyyamIHaMatch(new Date(2025, 2 - 1, 24, 18, 0, 0), 181, 4),
        new AyyamIHaMatch(new Date(2026, 2 - 1, 24, 18, 0, 0), 182, 5),
        new AyyamIHaMatch(new Date(2027, 2 - 1, 25, 18, 0, 0), 183, 4),
        new AyyamIHaMatch(new Date(2028, 2 - 1, 25, 18, 0, 0), 184, 4),
        new AyyamIHaMatch(new Date(2029, 2 - 1, 24, 18, 0, 0), 185, 4),
        new AyyamIHaMatch(new Date(2030, 2 - 1, 24, 18, 0, 0), 186, 4),
        new AyyamIHaMatch(new Date(2031, 2 - 1, 24, 18, 0, 0), 187, 5),

    ];

    for (let entry of ayyamIHaDays) {
        xit(`calculates Ayyam-i-Há correctly for ${entry.badiYear} with ${entry.numberOfIntercalaryDays} leap days`, () => {
            let badiDate : BadiDate;
            let calculatedBadiDate : BadiDate;
            let calculatedDateString : string;
            let expectedDateString : string;

            // fist day
            badiDate = new BadiDate(entry.badiYear, 19, 1);
            calculatedBadiDate = BadiDate.fromDateToBadiDate(entry.firstGregorianDate);
            calculatedDateString = `${calculatedBadiDate.getYear()}-${calculatedBadiDate.getMonth()}-${calculatedBadiDate.getDay()}`;
            expectedDateString = `${badiDate.getYear()}-${badiDate.getMonth()}-${badiDate.getDay()}`;
            expect(calculatedDateString).toBe(expectedDateString);

            // last day, add numberOfIntercalaryDays - 1
            badiDate = new BadiDate(entry.badiYear, 19, entry.numberOfIntercalaryDays);
            calculatedBadiDate = BadiDate.fromDateToBadiDate(entry.firstGregorianDate.addHours((entry.numberOfIntercalaryDays - 1) * 24));
            calculatedDateString = `${calculatedBadiDate.getYear()}-${calculatedBadiDate.getMonth()}-${calculatedBadiDate.getDay()}`;
            expectedDateString = `${badiDate.getYear()}-${badiDate.getMonth()}-${badiDate.getDay()}`;
            expect(calculatedDateString).toBe(expectedDateString);

            // first day of next month after Ayyám-I-Há, add one more day
            badiDate = new BadiDate(entry.badiYear, 20, 1);
            calculatedBadiDate = BadiDate.fromDateToBadiDate(entry.firstGregorianDate.addHours(24));
            calculatedDateString = `${calculatedBadiDate.getYear()}-${calculatedBadiDate.getMonth()}-${calculatedBadiDate.getDay()}`;
            expectedDateString = `${badiDate.getYear()}-${badiDate.getMonth()}-${badiDate.getDay()}`;
            expect(calculatedDateString).toBe(expectedDateString);

            console.log(calculatedDateString);
        });
    }

    it('calculates Anniversary of Bab and Bahá\'u\'lláh correctly');
});

/*

is all this needed?
@types/jasmine
jasmine-core
karma
karma-jasmine
karma-phantomjs-launcher
karma-webpack


*/