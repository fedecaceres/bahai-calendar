// based on https://github.com/moostafaa/Equinox

export enum SunEvent {
    VernalEquinox,
    SummerSolstice,
    AutumnalEquinox,
    WinterSolstice
}

export class Equinox {

    static readonly Deg2Radian : number = Math.PI / 180.0;

    public GetSunEventUtc(year : number, sunEvent : SunEvent) : Date
        {
            let y: number;
            let julianEphemerisDay: number;

            if (year >= 1000)
            {
                y = (Math.floor(year) - 2000) / 1000;

                switch (sunEvent)
                {
                    case SunEvent.VernalEquinox:
                        julianEphemerisDay = 2451623.80984 + 365242.37404 * y + 0.05169 * (y * y) - 0.00411 * (y * y * y) - 0.00057 * (y * y * y * y);
                        break;
                    case SunEvent.SummerSolstice:
                        julianEphemerisDay = 2451716.56767 + 365241.62603 * y + 0.00325 * (y * y) - 0.00888 * (y * y * y) - 0.00030 * (y * y * y * y);
                        break;
                    case SunEvent.AutumnalEquinox:
                        julianEphemerisDay = 2451810.21715 + 365242.01767 * y + 0.11575 * (y * y) - 0.00337 * (y * y * y) - 0.00078 * (y * y * y * y);
                        break;
                    case SunEvent.WinterSolstice:
                        julianEphemerisDay = 2451900.05952 + 365242.74049 * y + 0.06223 * (y * y) - 0.00823 * (y * y * y) - 0.00032 * (y * y * y * y);
                        break;
                    default:
                        return null;
                }
            }
            else
            {
                y = Math.floor(year) / 1000;

                switch (sunEvent)
                {
                    case SunEvent.VernalEquinox:
                        julianEphemerisDay = 1721139.29189 + 365242.13740 * y + 0.06134 * (y * y) - 0.00111 * (y * y * y) - 0.00071 * (y * y * y * y);
                        break;
                    case SunEvent.SummerSolstice:
                        julianEphemerisDay = 1721233.25401 + 365241.72562 * y + 0.05323 * (y * y) - 0.00907 * (y * y * y) - 0.00025 * (y * y * y * y);
                        break;
                    case SunEvent.AutumnalEquinox:
                        julianEphemerisDay = 1721325.70455 + 365242.49558 * y + 0.11677 * (y * y) - 0.00297 * (y * y * y) - 0.00074 * (y * y * y * y);
                        break;
                    case SunEvent.WinterSolstice:
                        julianEphemerisDay = 1721414.39987 + 365242.88257 * y + 0.00769 * (y * y) - 0.00933 * (y * y * y) - 0.00006 * (y * y * y * y);
                        break;
                    default:
                        return null;
                }
            }

            var julianCenturies = (julianEphemerisDay - 2451545.0) / 36525;

            var w = 35999.373 * julianCenturies - 2.47;

            var lambda = 1 + 0.0334 * Math.cos(w * Equinox.Deg2Radian) + 0.0007 * Math.cos(2 * w * Equinox.Deg2Radian);

            var sumOfPeriodicTerms = this.getSumOfPeriodicTerms(julianCenturies);

            // return new Date();

            return this.JulianToUtcDate(julianEphemerisDay + (0.00001 * sumOfPeriodicTerms / lambda));
        }

        private getSumOfPeriodicTerms(julianCenturies : number) : number
        {
            return 485 * Math.cos(Equinox.Deg2Radian * 324.96 + Equinox.Deg2Radian * (1934.136 * julianCenturies))
                   + 203 * Math.cos(Equinox.Deg2Radian * 337.23 + Equinox.Deg2Radian * (32964.467 * julianCenturies))
                   + 199 * Math.cos(Equinox.Deg2Radian * 342.08 + Equinox.Deg2Radian * (20.186 * julianCenturies))
                   + 182 * Math.cos(Equinox.Deg2Radian * 27.85 + Equinox.Deg2Radian * (445267.112 * julianCenturies))
                   + 156 * Math.cos(Equinox.Deg2Radian * 73.14 + Equinox.Deg2Radian * (45036.886 * julianCenturies))
                   + 136 * Math.cos(Equinox.Deg2Radian * 171.52 + Equinox.Deg2Radian * (22518.443 * julianCenturies))
                   + 77 * Math.cos(Equinox.Deg2Radian * 222.54 + Equinox.Deg2Radian * (65928.934 * julianCenturies))
                   + 74 * Math.cos(Equinox.Deg2Radian * 296.72 + Equinox.Deg2Radian * (3034.906 * julianCenturies))
                   + 70 * Math.cos(Equinox.Deg2Radian * 243.58 + Equinox.Deg2Radian * (9037.513 * julianCenturies))
                   + 58 * Math.cos(Equinox.Deg2Radian * 119.81 + Equinox.Deg2Radian * (33718.147 * julianCenturies))
                   + 52 * Math.cos(Equinox.Deg2Radian * 297.17 + Equinox.Deg2Radian * (150.678 * julianCenturies))
                   + 50 * Math.cos(Equinox.Deg2Radian * 21.02 + Equinox.Deg2Radian * (2281.226 * julianCenturies))
                   + 45 * Math.cos(Equinox.Deg2Radian * 247.54 + Equinox.Deg2Radian * (29929.562 * julianCenturies))
                   + 44 * Math.cos(Equinox.Deg2Radian * 325.15 + Equinox.Deg2Radian * (31555.956 * julianCenturies))
                   + 29 * Math.cos(Equinox.Deg2Radian * 60.93 + Equinox.Deg2Radian * (4443.417 * julianCenturies))
                   + 28 * Math.cos(Equinox.Deg2Radian * 155.12 + Equinox.Deg2Radian * (67555.328 * julianCenturies))
                   + 17 * Math.cos(Equinox.Deg2Radian * 288.79 + Equinox.Deg2Radian * (4562.452 * julianCenturies))
                   + 16 * Math.cos(Equinox.Deg2Radian * 198.04 + Equinox.Deg2Radian * (62894.029 * julianCenturies))
                   + 14 * Math.cos(Equinox.Deg2Radian * 199.76 + Equinox.Deg2Radian * (31436.921 * julianCenturies))
                   + 12 * Math.cos(Equinox.Deg2Radian * 95.39 + Equinox.Deg2Radian * (14577.848 * julianCenturies))
                   + 12 * Math.cos(Equinox.Deg2Radian * 287.11 + Equinox.Deg2Radian * (31931.756 * julianCenturies))
                   + 12 * Math.cos(Equinox.Deg2Radian * 320.81 + Equinox.Deg2Radian * (34777.259 * julianCenturies))
                   + 9 * Math.cos(Equinox.Deg2Radian * 227.73 + Equinox.Deg2Radian * (1222.114 * julianCenturies))
                   + 8 * Math.cos(Equinox.Deg2Radian * 15.45 + Equinox.Deg2Radian * (16859.074 * julianCenturies));
        }

        public JulianToUtcDate(julianDay : number) : Date
        {
            let a : number;
            let month : number, year : number;

            var j = julianDay + 0.5;
            var z = Math.floor(j);
            var f = j - z;

            if (z >= 2299161)
            {
                var alpha = Math.floor((z - 1867216.25) / 36524.25);
                a = z + 1 + alpha - Math.floor(alpha / 4);
            }
            else
                a = z;

            var b = a + 1524;

            var c = Math.floor((b - 122.1) / 365.25);

            var d = Math.floor(365.25 * c);

            var e = Math.floor((b - d) / 30.6001);

            var day = b - d - Math.floor(30.6001 * e) + f;

            if (e < 14)
                month = Math.floor(e - 1.0);
            else if (this.ApproxEquals(e, 14) || this.ApproxEquals(e, 15))
                month = Math.floor(e - 13.0);
            else
                throw new Error("Illegal month calculated.");

            if (month > 2)
                year = Math.floor(c - 4716.0);
            else if (month == 1 || month == 2)
                year = Math.floor(c - 4715.0);
            else
                throw new Error("Illegal year calculated.");

            //var span = TimeSpan.FromDays(day);
            let span : Date = new Date(day * 24 * 60 * 60 * 1000); // cheap way to face C#'s TimeSpan.fromDays
            let finalDate : Date = new Date(Date.UTC(year, month - 1, Math.floor(day), span.getUTCHours(), span.getUTCMinutes(), span.getUTCSeconds(), span.getMilliseconds()));

            return finalDate; // JS Date is 0-based

            //return new DateTime(year, month, (int)day, span.Hours, span.Minutes,
            //    span.Seconds, span.Milliseconds, new GregorianCalendar(), DateTimeKind.Utc);
        }

        public ApproxEquals(d1 : number, d2 : number) : boolean
        {
            let epsilon : number = 2.2204460492503131E-16;
            if (d1 == d2)
                return true;
            var tolerance = ((Math.abs(d1) + Math.abs(d2)) + 10.0) * epsilon;
            var difference = d1 - d2;
            return (-tolerance < difference && tolerance > difference);
        }
}