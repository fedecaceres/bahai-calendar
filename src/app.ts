import {BadiDate, DateElement, DateElementForm, DateName} from './calendar';
import {EventType, Holyday, Holydays} from './holydays';
import * as moment from 'moment';

// import {I18n} from 'i18next';
import I18n = require('i18next');

let currentLanguage = 'es';

let i18n = I18n.init({
    lng: currentLanguage,
    resources: {
        en: {
            translation: {
                "19_DAYS_FEAST": '19 Days Feast',
                'HOLY_DAY' : "Holy Day",
                'FEAST_OF' : "Feast of {{month}}",
            }
        },
        es: {
            translation: {
                "19_DAYS_FEAST": 'Fiesta de los 19 Días',
                'HOLY_DAY' : "Día Sagrado",
                'FEAST_OF' : "Fiesta de {{month}}",
                'Splendour' : 'Esplendor',
                'Glory' : 'Gloria',
                'Beauty' : 'Belleza',
                'Grandeur' : 'Grandeza',
                'Light' : 'Luz',
                'Mercy' : 'Misericordia',
                'Words' : 'Palabras',
                'Perfection' : 'Perfección',
                'Names' : 'Nombres',
                'Might' : 'Fuerza',
                'Will' : 'Volutand',
                'Knowledge' : 'Conocimiento',
                'Power' : 'Poder',
                'Speech' : 'Discurso',
                'Questions' : 'Preguntas',
                'Honour' : 'Honor',
                'Sovereignty' : 'Soberanía',
                'Dominion' : 'Dominio',
                'The Days of Há' : 'Días de Há',
                'Loftiness' : 'Sublimidad',

                'Naw-Rúz' : 'Naw-Rúz',
                'First day of Riḍván' : 'Primer día de Ridván',
                'Ninth day of Riḍván' : 'Noveno día de Ridván',
                'Twelfth day of Riḍván' : 'Duodécimo día de Ridván',
                'Declaration of the Báb' : 'Declaración del Báb',
                'Ascension of Bahá\'u\'lláh' : 'Ascensión de Bahá\'u\'lláh',
                'Martyrdom of the Báb' : 'Martirio del Báb',
                'Birth of the Báb' : 'Nacimiento del Báb',
                'Birth of Bahá\'u\'lláh' : 'Nacimiento de Bahá\'u\'lláh',
                'Day of the Covenant' : 'Día de la Alianza',
                'Ascension of `Abdu\'l-Bahá' : 'Ascención de `Abdu\'l-Bahá',

            }
        }
    }
});
// let nowGregorian = new Date(2017, 5, 3, 23, 0);

// let nowGregorian = new Date(2017, 6, 12, 0, 0, 0);
let nowGregorian = new Date();
let date = BadiDate.fromDateToBadiDate(nowGregorian);
let thisMoment = moment(nowGregorian).locale(currentLanguage);
let colors: string[] = [
    'red',
    'pink',
    'purple',
    'deep-purple',
    'indigo',
    'blue',
    'light-blue',
    'cyan',
    'teal',
    'green',
    'light-green',
    'lime',
    'yellow',
    'amber',
    'orange',
    'deep-orange',
    'brown',
    'grey'
];

function generateInDaysSegment(duration: string): string {
    return `<div class="mdl-color-text--grey" style="margin: 16px 0; font-weight: bold;">${duration}</div>`;
}
function generateHolydayCard(holyday: Holyday): string {
    let cardColor = colors[(holyday.date.getMonth() - 1) % 18];
    let textColor = 'white';
    let holydayType = holyday.type == EventType.NineteenDayFeast ? '19_DAYS_FEAST' : 'HOLY_DAY';
    let holydayName = holydayType == '19_DAYS_FEAST' ? i18n.t('FEAST_OF', {'month' : i18n.t(holyday.name)}) : i18n.t(holyday.name) ;
    let date = moment(holyday.date.getTime()).locale(currentLanguage).format('LLLL');
    let style = '';

    if (holyday.name.includes('ván')) {
//         style = `background: url(https://img.clipartfest.com/1cbfe80f42d26f613465116d8a19f665_1000-images-about-logo-ideas-free-rose-vector-clipart_1024-765.jpeg);
//     background-size: cover;
//     background-blend-mode: darken;
//     background-repeat: no-repeat;
//     background-position: 80px center;
// `;
    }

    return `<div class="mdl-card mdl-color--${cardColor} mdl-shadow--2dp" style="min-height: auto; ${style}; margin: 16px 0;">
    <div class="mdl-color-text--${textColor} mdl-card__normal-text">
        <p><strong>${date}</strong></p>
        <h4 style="margin-top: 12px">${i18n.t(holydayName)}</h4>
        ${i18n.t(holydayType)}
    </div>
</div>`
}

let cardColor = colors[(date.getMonth() - 1) % 18];
document.getElementById('main-card').className += ` mdl-color--${cardColor}`;

document.getElementById('gregorian-date').innerHTML = `${thisMoment.format('LL')}`;
document.getElementById('day-number').innerHTML = `${date.getDay()}`;
document.getElementById('month-persian-latin').innerHTML = `${date.getDateElement(DateElement.MONTH, DateElementForm.ARABIC_LATIN)}`;
document.getElementById('month-user-locale').innerHTML = `${i18n.t(date.getDateElement(DateElement.MONTH, DateElementForm.ENGLISH))}`;
document.getElementById('year').innerHTML = `${date.getYear()} E.B.`;


// document.getElementById('date4').innerHTML = `${i18n.t('hi')}`;

// document.getElementById('loader').setAttribute("hidden", "");
// document.getElementById('content').removeAttribute("hidden");

let hs = new Holydays();
let holydays: Holyday[] = hs.between(date, date.addDays(19*19));
let html = '';
let lastDuration = '', duration : string;
for (let holyday of holydays) {
    let delta = holyday.date.getTime() - nowGregorian.getTime();
    if (delta <= 0) {
        duration = 'now';
    } else {
        duration = moment.duration(delta).locale(currentLanguage).humanize(true);
    }
    if (lastDuration !== duration) {
        lastDuration = duration;
        html += generateInDaysSegment(duration);
    }

    html += generateHolydayCard(holyday);
}
document.getElementById('holydays-here').innerHTML = html;

// SERVICE WORKER STUFF!

if ('serviceWorker' in navigator) {
  window.addEventListener('load', function() {
    navigator.serviceWorker.register('sw.js').then(function(registration) {
      // Registration was successful
      console.log('ServiceWorker registration successful with scope: ', registration.scope);
    }).catch(function(err) {
      // registration failed :(
      console.log('ServiceWorker registration failed: ', err);
    });
  });
}
