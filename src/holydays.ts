import { BadiDate, MONTH_NAMES } from './calendar';

export enum EventType {
    NineteenDayFeast,
    SacredDate
};

// check this out:
// https://github.com/mourner/suncalc
// could be useful

class BadiDateMatcher {
    day: number;
    month: number;
    holyday: Holyday

    constructor(day: number, month: number, holyday: Holyday) {
        this.day = day;
        this.month = month;
        this.holyday = holyday;
    }

    public matches(date: BadiDate) {
        if (date.getDay() == this.day && date.getMonth() == this.month) {
            return true;
        }
    }
}

// dates that match to badi day and month
// dates that match the 8th new moon after Naw-Rúz
export class Holyday {
    name: string;
    type: EventType;
    date: BadiDate;

    constructor(name: string, type: EventType, date?: BadiDate) {
        this.name = name;
        this.type = type;
        this.date = date;
    }
}

let holydayPatterns = [
    new BadiDateMatcher(1, 1, new Holyday('Naw-Rúz', EventType.SacredDate)),
    new BadiDateMatcher(13, 2, new Holyday('First day of Riḍván', EventType.SacredDate)), // Special time of celebration or commemoration (3pm)
    new BadiDateMatcher(2, 3, new Holyday('Ninth day of Riḍván', EventType.SacredDate)),
    new BadiDateMatcher(5, 3, new Holyday('Twelfth day of Riḍván', EventType.SacredDate)),
    new BadiDateMatcher(8, 4, new Holyday('Declaration of the Báb', EventType.SacredDate)),
    new BadiDateMatcher(13, 4, new Holyday('Ascension of Bahá\'u\'lláh', EventType.SacredDate)),
    new BadiDateMatcher(17, 6, new Holyday('Martyrdom of the Báb', EventType.SacredDate)),
    // new BadiDateMatcher(1, 1, new Holyday('Birth of the Báb', EventType.SacredDate)),
    // new BadiDateMatcher(1, 1, new Holyday('Birth of Bahá\'u\'lláh', EventType.SacredDate)),
    new BadiDateMatcher(4, 14, new Holyday('Day of the Covenant', EventType.SacredDate)),
    new BadiDateMatcher(6, 14, new Holyday('Ascension of `Abdu\'l-Bahá', EventType.SacredDate))
];

export class Holydays {


    /**
     * List of 
     * @param badiDateStart 
     * @param badiDateEnd 
     */
    public between(badiDateStart: BadiDate, badiDateEnd: BadiDate): Holyday[] {
        let holyDays = [];
        let match: Holyday;
        let currentBadiDate = badiDateStart;

        // if we're in the middle of the day before sunset or on the day after sunset
        if (currentBadiDate.getHours() < 18) {
            // go back to the start of the current day
            currentBadiDate = currentBadiDate.addDays(-1);
        } else {
            // do nothing, we're already on the day we need to be in
        }
        // reset hour to 18;
        currentBadiDate.setHours(18);
        currentBadiDate.setMinutes(0);
        currentBadiDate.setSeconds(0);

        // we're interested in the NEXT event, so we move forward one day and we start from there
        // currentBadiDate = currentBadiDate.addDays(1);

        while (currentBadiDate.isBefore(badiDateEnd)) {
            match = this.on(currentBadiDate);
            if (match != null) {
                holyDays.push(match);
            }
            currentBadiDate = currentBadiDate.addDays(1);
        }
        return holyDays;
    }

    public on(badiDate: BadiDate): Holyday {
        let holyday: Holyday;
        holyday = this.isSacredDay(badiDate) || this.is19DF(badiDate);
        return holyday;
    }

    public isHolyday(badiDate: BadiDate) {
        return this.on(badiDate) != null;
    }

    private is19DF(badiDate: BadiDate): Holyday {
        // first day and not Ayyám-i-Há (month 19)
        if (badiDate.getDay() == 1 && badiDate.getMonth() != 19) {
            return new Holyday(MONTH_NAMES[badiDate.getMonth() - 1].englishName, EventType.NineteenDayFeast, badiDate);
        }
        return null;
    }

    private isSacredDay(badiDate: BadiDate): Holyday {
        for (let holydayPattern of holydayPatterns) {
            if (holydayPattern.matches(badiDate)) {
                return new Holyday(holydayPattern.holyday.name, holydayPattern.holyday.type, badiDate);
            }
        }
        return null;
    }


}