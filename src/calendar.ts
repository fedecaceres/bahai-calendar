import { Equinox, SunEvent } from "./equinox";
import * as moment from 'moment';

/*
Operations required:
- get current date as Badi Date
- get list of 19 day feasts
- get list of sacred day in a given range
- 
*/

declare global {
    interface Date {
        addHours(h: number): Date;
    }
}

Date.prototype.addHours = function (h: number): Date {
    this.setTime(this.getTime() + (h * 60 * 60 * 1000));
    return this;
}

export enum DateElement {
    YEAR,
    MONTH,
    DAY
}

export enum DateElementForm {
    ENGLISH, ARABIC, ARABIC_LATIN
}

export class DateName {
    englishName: string;
    arabicScript: string;
    arabicName: string;

    constructor(arabicName: string, arabicScrit: string, englishName: string) {
        this.englishName = englishName;
        this.arabicScript = arabicScrit;
        this.arabicName = arabicName;
    }
}

export let MONTH_NAMES: DateName[] = [
    new DateName('Bahá', 'بهاء', 'Splendour'),
    new DateName('Jalál', 'جلال', 'Glory'),
    new DateName('Jamál', 'جمال', 'Beauty'),
    new DateName('‘Aẓamat', 'عظمة', 'Grandeur'),
    new DateName('Núr', 'نور', 'Light'),
    new DateName('Raḥmat', 'رحمة', 'Mercy'),
    new DateName('Kalimát', 'كلمات', 'Words'),
    new DateName('Kamál', 'كمال', 'Perfection'),
    new DateName('Asmá’', 'اسماء', 'Names'),
    new DateName('‘Izzat', 'عزة', 'Might'),
    new DateName('Mashíyyat', 'مشية', 'Will'),
    new DateName('‘Ilm', 'علم', 'Knowledge'),
    new DateName('Qudrat', 'قدرة', 'Power'),
    new DateName('Qawl', 'قول', 'Speech'),
    new DateName('Masá’il', 'مسائل', 'Questions'),
    new DateName('Sharaf', 'شرف', 'Honour'),
    new DateName('Sulṭán', 'سلطان', 'Sovereignty'),
    new DateName('Mulk', 'ملك', 'Dominion'),
    new DateName('Ayyám-i-Há', 'ايام الهاء', 'The Days of Há'),
    new DateName('‘Alá’', 'علاء', 'Loftiness')
];

export class BadiDate {
    year: number;
    month: number;
    day: number;
    hours: number;
    minutes: number;
    seconds: number;
    time: number;

    constructor(year: number, month: number, day: number, hours = 18, minutes = 0, seconds = 0, time?: number) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
        this.time = typeof time === 'undefined' ? this.fromBadiToTime(this) : time;
    }

    private static nawRuzDaysByYear: { [key:number]:number; } = {
        // add previous up until 1944
        2015: 21,
        2016: 20,
        2017: 20,
        2018: 21,
        2019: 21,
        2020: 20,
        2021: 20,
        2022: 21,
        2023: 21,
        2024: 20,
        2025: 20,
        2026: 21,
        2027: 21,
        2028: 20,
        2029: 20,
        2030: 20,
        // todo: add more here, obviously
    };

    public static getNawRuzForGregorianYear(year: number) {
        return new Date(
            year,
            3 - 1,
            BadiDate.nawRuzDaysByYear[year] - 1,
            18,
            0,
            0,
            0
        );
    }

    public static fromDateToBadiDate(date: Date) {

        let bd: BadiDate = new BadiDate(1844, 1, 1, 0, 0, 0, date.getTime()); // contructing invalid date and then using setter does not seem like a good idea...
        bd.setDate(date);
        return bd;
    }

    private fromBadiToTime(badiDate: BadiDate): number {
        let time: number = 0;
        let days: number = 0;

        let nawRuzStart: Date = BadiDate.getNawRuzForGregorianYear(badiDate.getYear() + 1843);
        let numberOfIntercalaryDays = nawRuzStart.getDate() == 19 && BadiDate.getNawRuzForGregorianYear(nawRuzStart.getFullYear() + 1).getDate() == 20 ? 5 : 4;

        if (badiDate.getMonth() <= 19) {
            days = ((badiDate.getMonth() - 1) * 19) + badiDate.getDay() - 1;
        } else {
            days = ((badiDate.getMonth() - 1) * 19) + numberOfIntercalaryDays + badiDate.getDay() - 1;
        }

        let milliseconds = (((badiDate.getHours() * 60 + badiDate.getMinutes()) * 60 + badiDate.getSeconds()) * 1000)
                         - (((nawRuzStart.getHours() * 60 + nawRuzStart.getMinutes()) * 60 + nawRuzStart.getSeconds()) * 1000);

        time = nawRuzStart.getTime() + (days * 24 * 60 * 60 * 1000) + milliseconds;
        // time = time + badiDate.getTime() - time;


        // let hours = nawRuzStart.getHours() - badiDate.getHours();
        // let minutes = nawRuzStart.getMinutes() - badiDate.getMinutes();
        // let seconds = nawRuzStart.getSeconds() - badiDate.getSeconds();

        // // convert hours, minutes and seconds
        // time = nawRuzStart.getTime() + (((((days * 24) + hours) * 60 + minutes) * 60 + seconds) * 1000);
        // debugger;
        // console.log(badiDate, days, time);

        return time;
    }

    public toDate(): Date {
        return new Date(this.time);
    }

    /**
     * Returns a new BadiDate with the specified hours offset
     * @param n number of hours
     */
    public addDays(d: number): BadiDate {
        return BadiDate.fromDateToBadiDate(moment(this.toDate()).add(d, 'days').toDate());
    }

    public isBefore(d: BadiDate): boolean {
        return this.time < d.getTime();
        // return this.getYear() <= d.getYear()
        //     && this.getMonth() <= d.getMonth()
        //     && this.getDay() <= d.getDay()
        //     && this.getHours() <= d.getHours()
        //     && this.getMinutes() <= d.getMinutes()
        //     && this.getSeconds() < d.getSeconds();
    }

    public toString(): string {
        return `${this.year}/${this.month}/${this.day} ${this.hours}:${this.minutes}:${this.seconds}`;
    }

    public getTextualRepresentation(): string {
        return `${this.year}/${MONTH_NAMES[this.month - 1].englishName}/${this.day} ${this.hours}:${this.minutes}:${this.seconds}`;
    }

    public getDateElement(dateElement: DateElement, form: DateElementForm): string {
        let name: DateName = null;
        switch (dateElement) {
            case DateElement.MONTH:
                name = MONTH_NAMES[this.month - 1];
            default:
                break;
        }

        if (name != null) {
            switch (form) {
                case DateElementForm.ENGLISH:
                    return name.englishName;
                case DateElementForm.ARABIC:
                    return name.arabicScript;
                case DateElementForm.ARABIC_LATIN:
                    return name.arabicName;
            }
        }

        throw new Error("Not supported");
    }

    public getYear(): number {
        return this.year;
    }

    public getMonth(): number {
        return this.month;
    }

    public getDay(): number {
        return this.day;
    }

    public setDay(day: number) {
        this.day = day;
    }

    public getHours(): number {
        return this.hours
    }

    public setHours(hours: number) {
        this.hours = hours;
        let x = this.toDate();
        x.setHours(hours);
        // changing hours can change day, recalc badi date
        this.setTime(x.getTime());
        // this.time = x.getTime();
        // this.refreshTime();
    }

    public getMinutes(): number {
        return this.minutes;
    }

    public setMinutes(minutes: number) {
        this.minutes = minutes;
        let x = this.toDate();
        x.setMinutes(minutes);
        // just update internal time, no need to recalc
        this.time = x.getTime();
    }

    public getSeconds(): number {
        return this.seconds;
    }

    public setSeconds(seconds: number) {
        this.seconds = seconds;
        let x = this.toDate();
        x.setSeconds(seconds);
        // just update internal time, no need to recalc
        this.time = x.getTime();
    }

    public getTime(): number {
        return this.time;
    }

    public setDate(date: Date) {
        this.setTime(date.getTime());
    }

    public setTime(time: number) {
        let date: Date = new Date(time);

        // current date
        // let localTimeInUTC = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), date.getMilliseconds()));
        let localTime = new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), date.getMilliseconds());
        let nawRuzStart: Date = BadiDate.getNawRuzForGregorianYear(localTime.getFullYear()); // TODO confirm if year is fine ... or UTC year?
        let delta = localTime.getTime() - nawRuzStart.getTime();
        if (delta < 0) {
            // for now, we're not gonna work with negative deltas, they could be useful, but they add additional complexity that is out of scope for now
            nawRuzStart = BadiDate.getNawRuzForGregorianYear(localTime.getFullYear() - 1);
            delta = localTime.getTime() - nawRuzStart.getTime();
        }

        let badiYear = nawRuzStart.getFullYear() - 1843; // 1844 AD is year 1 BE

        let numberOfIntercalaryDays = nawRuzStart.getDate() == 19 && BadiDate.getNawRuzForGregorianYear(nawRuzStart.getFullYear() + 1).getDate() == 20 ? 5 : 4;
        let badiDay = 0;
        let badiMonth = 0;
        let remainingDays = Math.floor(delta / 1000 / 60 / 60 / 24);
        if (remainingDays < 19 * 18) {
            badiDay = (remainingDays % 19) + 1;
            badiMonth = Math.floor(remainingDays / 19) + 1;
        } else if (remainingDays < 19 * 18 + numberOfIntercalaryDays) {
            badiDay = (remainingDays % 19) + 1;
            badiMonth = Math.floor(remainingDays / 19) + 1;
        } else {
            badiDay = (remainingDays - (19 * 18 + numberOfIntercalaryDays)) + 1;
            badiMonth = 20;
        }

        this.year = badiYear;
        this.month = badiMonth;
        this.day = badiDay;
        this.hours = localTime.getHours();
        this.minutes = localTime.getMinutes();
        this.seconds = localTime.getSeconds();
        this.time = time;

        // let bd: BadiDate = new BadiDate(badiYear, badiMonth, badiDay, localTime.getHours(), localTime.getMinutes(), localTime.getSeconds(), date.getTime());

    }

    /**
     * Refresh the internal time object from the Badi date elements
     * TODO: This seems unoptimal... could make it faster
     */
    public refreshTime() {
        this.time = this.fromBadiToTime(this);
    }
}
