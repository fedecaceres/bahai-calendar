var webpackConfig = require('./webpack.config');

module.exports = function (config) {
    config.set({
        frameworks: ["jasmine"],
        files: [
            // { pattern: "src/**/*.ts" }, // *.tsx for React Jsx
            { pattern: "test/**/*.ts" }, // *.tsx for React Jsx
        ],
        preprocessors: {
            "**/*.ts": ["webpack"], // *.tsx for React Jsx
        },
        webpack: {
            module: webpackConfig.module,
            resolve: webpackConfig.resolve
        },
        // reporters: ["progress"],
        reporters: ["spec"],
        specReporter: {
            maxLogLines: 5,         // limit number of lines logged per test
            suppressErrorSummary: false,  // do not print error summary
            suppressFailed: false,  // do not print information about failed tests
            suppressPassed: false,  // do not print information about passed tests
            suppressSkipped: false,  // do not print information about skipped tests
            showSpecTiming: false // print the time elapsed for each spec
        },
        // no need to specify specific plugins as long as they all are named karma-*
        // e.g.: karma-jasmine, karma-spec-reporter, etc...
        // plugins: ["karma-spec-reporter"],
        // browsers: ["Chromium"],
        browsers: ["PhantomJS"],
        customLaunchers:{
            HeadlessChrome:{
                base: 'ChromeHeadless',
                flags: [
                    '--no-sandbox',
                    '--headless',
                    '--disable-gpu',
                    '--disable-translate',
                    '--disable-extensions',
                    '--remote-debugging-port=9222'
                ]
            }
        },
        colors: true,
        logLevel: config.LOG_ERROR,
            autoWatch: true,
        // singleRun: true,
        concurrency: Infinity,





        // frameworks: ['jasmine'],
        // files: [
        // 'test/test_index.ts'
        // ],
        // preprocessors: {
        //   "**/*.ts": ["webpack"],
        // }
    });
};